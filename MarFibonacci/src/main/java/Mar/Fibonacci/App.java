package Mar.Fibonacci;

import java.util.Scanner;

public class App {

    private static Scanner in = new Scanner(System.in);
    private static Fibonacci fibonachi = new Fibonacci();
    private static UserComunication communication = new UserComunication();

    public static int readNumber() {
        return in.nextInt();
    }

    public static void println() {
        System.out.println();
    }

    public static void print(Object text) {
        System.out.print(String.valueOf(text));
    }

    public static void println(Object text) {
        System.out.println(String.valueOf(text));
    }


    private static boolean ShowFibonacciMenu() {
        FibonacciMenu resultFibonacciMenu = communication.showFibonacciMenu();
        switch (resultFibonacciMenu) {
            case GenerateByCount:
                fibonachi.generateByCount(communication.GetCount());
                break;
            case GenerateByInterval:
                do{
                    communication.GetIntervals();
                }while(communication.startInterval > communication.endInterval);
                fibonachi.generateNumbersByInterval(communication.startInterval, communication.endInterval);
                break;
            case Exit:
                return false;
        }

        println("Numbers generated successfully");
        return true;
    }

    public static boolean ShowMainMenu() {
        MainMenu userMainMenuChoise = communication.showMenu();

        switch (userMainMenuChoise) {
            case PrintNumbersAsceling:
                fibonachi.printNumbers(true);
                break;

            case PrintNumbersDesceling:
                fibonachi.printNumbers(false);
                break;

            case PrintSumOddNumbers:
                fibonachi.printSumOddNumbers();
                break;

            case PrintSumEvenNumbers:
                fibonachi.printSumEvenNumbers();
                break;

            case PrintBiggestOddNumber:
                fibonachi.printBiggestOddNumber();
                break;

            case PrintBiggestEvenNumber:
                fibonachi.printBiggestEvenNumber();
                break;

            case PrintPercentageOddNumbers:
                fibonachi.printPercentageOddNumbers();
                break;

            case PrintPercentageEvenNumbers:
                fibonachi.printPercentageEvenNumbers();
                break;

            case Back:
                return false;
        }

        return true;
    }

    public static void main(String[] args) {

        do {
            if (!ShowFibonacciMenu())
                return;

            while (ShowMainMenu()) ;
        } while (true);
    }
}


