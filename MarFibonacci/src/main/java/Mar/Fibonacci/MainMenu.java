package Mar.Fibonacci;

public enum MainMenu {
    PrintNumbersAsceling,
    PrintNumbersDesceling,
    PrintSumOddNumbers,
    PrintSumEvenNumbers,
    PrintBiggestOddNumber,
    PrintBiggestEvenNumber,
    PrintPercentageOddNumbers,
    PrintPercentageEvenNumbers,
    Back
}
