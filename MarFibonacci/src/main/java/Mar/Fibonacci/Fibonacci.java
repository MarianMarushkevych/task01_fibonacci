package Mar.Fibonacci;

import java.util.ArrayList;
import java.util.Scanner;

public class Fibonacci {
    private Scanner scanner;
    int start, end;

    ArrayList<Integer> numbers = new ArrayList<>();

    public void generateNumbersByInterval(int start, int end)
    {
        numbers.clear();

        this.start =  start;
        this.end = end;

        int t1,t2;
        t1 = 0;
        putIntoArrayInterval(t1);

        t2 = 1;
        putIntoArrayInterval(t2);

        while (t2 < end)
        {
            t1 = t1 + t2;
            putIntoArrayInterval(t1);

            if (t1 > end) break;

            t2 = t1 + t2;
            putIntoArrayInterval(t2);
        }
    }

    public void generateByCount(int count)
    {
        numbers.clear();
        if(count < 1) return;

        int t1,t2;
        t1 = 0;
        numbers.add(t1);
        count--;
        if (count < 1) return;

        t2 = 1;
        numbers.add(t2);
        count--;
        if (count < 1) return;

        while (count > 0)
        {
            t1 = t1 + t2;
            numbers.add(t1);
            count--;

            if (count == 0) break;

            t2 = t1 + t2;
            numbers.add(t2);
            count--;
        }
    }

    public int getNumberCount()
    {
        return numbers.size();
    }

    private boolean canAddElementInterval(int data)
    {
        return data >= start && data <= end;
    }

    private void putIntoArrayInterval(int data)
    {
        if(canAddElementInterval(data))
            numbers.add(data);
    }

    void printNumbers(Boolean upscaling)
    {
        App.print("Numbers:\t");
        if(upscaling)
            for(Object number : numbers){
                App.print(number + " ");
            }
        else
            for(int i = numbers.size()-1; i != 0; i--)
            {
                App.print(numbers.get(i) + " ");
            }
    }

    public void printSumOddNumbers()
    {
        int summ = 0;

        for(int number : numbers){

            if (number % 2 != 0)
                summ += number;
        }

        App.print("Summ of odd numbers:\t" + summ);
    }

    public void printSumEvenNumbers()
    {
        int summ = 0;

        for(int number : numbers){

            if (number % 2 == 0)
                summ += number;
        }

        App.print("Summ of even numbers:\t" + summ);
    }

    public void printBiggestOddNumber()
    {
        int max = 0;

        for(int number : numbers){

            if (number % 2 != 0 && max < number)
                max = number;
        }

        App.print("Biggest odd number:\t" + max);
    }

    public void printBiggestEvenNumber()
    {
        int max = 0;

        for(int number : numbers){

            if (number % 2 == 0 && max < number)
                max = number;
        }

        App.print("Biggest even number:\t" + max);
    }

    public void printPercentageOddNumbers()
    {
        int count = 0;

        for(int number : numbers){

            if (number % 2 != 0)
                count++;
        }

        App.print("Percentage of odd numbers:\t" + (float)count/numbers.size()*100);
    }

    public void printPercentageEvenNumbers() {
        int count = 0;

        for(int number : numbers){

            if (number % 2 == 0)
                count++;
        }

        App.print("Percentage of odd numbers:\t" + (float)count/numbers.size()*100);
    }

}
