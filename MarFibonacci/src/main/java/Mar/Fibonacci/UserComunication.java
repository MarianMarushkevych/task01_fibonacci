package Mar.Fibonacci;

public class UserComunication {

    public int startInterval, endInterval;

    public void GetIntervals()
    {
        App.println("Please set interval:");

        App.print("Start = ");
        startInterval = App.readNumber();

        App.print("End = ");
        endInterval = App.readNumber();
    }

    public FibonacciMenu showFibonacciMenu()
    {
        int result = 0;
        do {
            App.println(">> Generation of Fibonacci <<");
            App.println("1. Generate by interval");
            App.println("2. Generate by count");
            App.println("3. Exit");

            App.print("Enter choice:\t");
            result = App.readNumber();

        }while (result < 1 || result > 3);

        return FibonacciMenu.values()[result -1];
    }

    public MainMenu showMenu()
    {
        int result = 0;
        do {
            App.println(">> Menu <<");
            App.println("1.\tPrint numbers");
            App.println("2.\tPrint numbers desceling");
            App.println("3.\tPrint sum of odd numbers");
            App.println("4.\tPrint sum of even numbers");
            App.println("5.\tPrint biggest odd number");
            App.println("6.\tPrint biggest even number");
            App.println("7.\tPrint percentage of odd numbers");
            App.println("8.\tPrint percentage of even numbers");
            App.println("9.\tBack to fibonacci menu");

            App.print("Enter choice:\t");
            result = App.readNumber();

        }while (result < 1 || result > 9);

        return MainMenu.values()[result - 1];
    }

    public int GetCount() {

        int result = 0;
        do {
            App.print("Enter count of fibonacci numbers :\t");
            result = App.readNumber();

        } while (result < 1);

        return result;
    }
}
